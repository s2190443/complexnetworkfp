import networkx as nx
import matplotlib.pyplot as plt

RANDOM_SEED = 772

if __name__ == '__main__':
    n = 100  # number of vertexes. This is changed according to the desired experiment
    # print(er_graph.number_of_edges())
    rg_clique_list = []  # list to store the number of cliques for each generated graph GeRG
    er_clique_list = []  # list to store the number of cliques for each generated graph ERG
    th = 2  # threshold value for connecting two nodes in GeRG
    for d in range(2, 30):  # loop to generate geometric random graphs
        print(d)  # d is the number of dimensions for the Geometric random Graph
        rg_graph = nx.random_geometric_graph(n, th, d,
                                             seed=RANDOM_SEED)  # generate a GeRG with n vertexes, th threshold and d
        # dimensions
        rg_clique_list.append(nx.graph_number_of_cliques(rg_graph))
    for lmbd in range(2, 30):  # loop to generate ER graphs
        print(lmbd)
        er_graph = nx.erdos_renyi_graph(n, (lmbd / 10) / n, seed=RANDOM_SEED)  # generate ER graph with n vertexes and
        # lmb/10/n edge probability. Division by 10 is done, so we generate as many graphs as there are dimensions in
        # GeRG while keeping the edge creation probability small enough
        er_clique_list.append(nx.graph_number_of_cliques(er_graph))
    # the code below plots the results
    fig, ax = plt.subplots()
    ax.plot(range(2, len(rg_clique_list) + 2), rg_clique_list, label="GeRG")
    ax.plot(range(2, len(er_clique_list) + 2), er_clique_list, label="ERG")
    ax.set(xlabel="dimensions, probability index", ylabel="number of cliques", title="Number of (maximal) cliques in "
                                                                                     "GeRG and ERG")
    ax.legend()
    ax.grid()
    plt.show()
